#include "queue.h"
#include <stdio.h>

int main(int argc, char* argv[])
{
    QUEUE* queue = initQueue(50, sizeof(int), 50);
    for (int i=0; i<30; i++) {
        queue->enqueue(queue, &i);
        int a;
        !queue->current(queue, &a) && printf("%d\n", a);
    }
    for (int i=0; i<20; i++) {
        queue->dequeue(queue);
        int a;
        !queue->current(queue, &a) && printf("%d\n", a);
    }
    for (int i=30; i<80; i++) {
        queue->enqueue(queue, &i);
        int a;
        !queue->current(queue, &a) && printf("%d\n", a);
    }
    for (int i=0; i<70; i++) {
        queue->dequeue(queue);
        int a;
        !queue->current(queue, &a) && printf("%d\n", a);
    }
    freeQueue(queue);
    return 0;
}
