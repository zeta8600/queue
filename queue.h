#ifndef _QUEUE_H_20170605_
#define _QUEUE_H_20170605_

#include <stddef.h>

typedef struct QUEUE{
    struct _queue * const vars;
    int (* const enqueue)(struct QUEUE*, const void*);
    int (* const dequeue)(struct QUEUE*);
    int (* const current)(struct QUEUE*, void*const);
} QUEUE;

QUEUE* initQueue(size_t, size_t, size_t);
int freeQueue(QUEUE*);

#endif
