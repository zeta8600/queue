#include "queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct _queue{
    size_t head;
    size_t tail;
    size_t size;
    size_t num;
    size_t numInc;
    void* array;
    bool isNothing;
};

static bool isNullPtr(QUEUE* queue)
{
    if (queue == NULL) return true;
    if (queue->vars == NULL) return true;
    if (queue->vars->array == NULL) return true;
    return false;
}

static int queueEnqueue(QUEUE* queue, const void* data)
{
    if (isNullPtr(queue)) {
        freeQueue(queue);
        return -1;
    }

    if (queue->vars->isNothing) {
        memcpy(
            queue->vars->array + queue->vars->tail * queue->vars->size,
            data,
            queue->vars->size
        );
        queue->vars->isNothing = false;
        return 0;
    }

    queue->vars->tail++;

    if (queue->vars->tail == queue->vars->head) {
        void *newArray = realloc(queue->vars->array,
                            queue->vars->size * queue->vars->num);
        if (newArray == NULL) {
            freeQueue(queue);
            return -1;
        }
        queue->vars->array = newArray;
        memmove(
            queue->vars->array + (queue->vars->head + queue->vars->numInc)
              * queue->vars->size,
            queue->vars->array + queue->vars->head * queue->vars->size,
            (queue->vars->num - queue->vars->head) * queue->vars->size
        );
        queue->vars->num += queue->vars->numInc;
        queue->vars->head += queue->vars->numInc;
    }

    queue->vars->tail %= queue->vars->num;

    if (queue->vars->tail == queue->vars->head) {
        queue->vars->tail += queue->vars->num;
        void *newArray = realloc(queue->vars->array,
                            queue->vars->size * queue->vars->num);
        if (newArray == NULL) {
            freeQueue(queue);
            return -1;
        }
        queue->vars->array = newArray;
        queue->vars->num += queue->vars->numInc;
    }

    memcpy(
        queue->vars->array + queue->vars->tail * queue->vars->size,
        data,
        queue->vars->size
    );

    return 0;
}

static int queueDequeue(QUEUE* queue)
{
    if (queue->vars->head == queue->vars->tail) {
        if (queue->vars->isNothing) {
            return -1;
        } else {
            queue->vars->isNothing = true;
            return 0;
        }
    }

    queue->vars->head++;
    queue->vars->head %= queue->vars->num;

    return 0;
}

static int queueCurrent(QUEUE* queue, void*const out)
{
    if (isNullPtr(queue)) {
        freeQueue(queue);
        return -1;
    }
    if (queue->vars->isNothing) return -1;

    memcpy(
        out,
        queue->vars->array + queue->vars->head * queue->vars->size,
        queue->vars->size
    );
    return 0;
}

QUEUE* initQueue(size_t num, size_t size, size_t numInc)
{
    QUEUE *queue = (QUEUE*)malloc(sizeof(QUEUE));
    if (queue == NULL) return NULL;

    struct _queue *vars = (struct _queue*)malloc(sizeof(struct _queue));
    if (vars == NULL) {
        free(queue);
        return NULL;
    }

    vars->array = calloc(num, size);
    if (vars->array == NULL) {
        free(vars);
        free(queue);
        return NULL;
    }

    if (size*num == 0) {
        free(vars->array);
        free(vars);
        free(queue);
        return NULL;
    }
    vars->size = size;
    vars->num = num;
    if (numInc != 0) vars->numInc = numInc;
    else vars->numInc = num;
    vars->head = 0;
    vars->tail = 0;
    vars->isNothing = true;

    QUEUE srcQueue = {vars, queueEnqueue, queueDequeue, queueCurrent};
    memcpy(queue, &srcQueue, sizeof(QUEUE));

    return queue;
}

int freeQueue(QUEUE* queue)
{
    if (queue == NULL) return -1;
    if (queue->vars == NULL) {
        free(queue);
        queue = NULL;
        return -1;
    }
    if (queue->vars->array == NULL) {
        free(queue->vars);
        free(queue);
        queue = NULL;
        return -1;
    }
    free(queue->vars->array);
    free(queue->vars);
    free(queue);
    queue = NULL;
    return 0;
}
